document.querySelector('.cross-btn').style.display = 'none';

document.querySelector('.hamburger').addEventListener('click', () => {
    document.querySelector('.sidebar').classList.toggle('remove-sidebar');
    if(document.querySelector('.sidebar').classList.contains('remove-sidebar')) {
        document.querySelector('.menu-btn').style.display = 'inline';
        document.querySelector('.cross-btn').style.display = 'none';
    } else {
        setTimeout(()=> {
            document.querySelector('.cross-btn').style.display = 'inline';
        }, 300);
        document.querySelector('.menu-btn').style.display = 'none';
}
})